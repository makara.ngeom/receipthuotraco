/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,  
  ScrollView,
  Image,
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

export default class FirstStockCollection extends Component {
    constructor(props){
        super(props);
        this.state = {
            date: new Date(),
            tableHead: ['Collectebd Stock', 'QTY', 'Price', 'Total'],
            tableData: [
                ['FINE KS Carton', '1000', '$7.650', '$76.500'],
                ['WESTFF Carton', '8', '$3.000', '$29.600'],
                ['WESTM Carton', '6', '$3.700', '$22.200'],
            ],
            tableDFooter: ['TOTAL', '24', '', '$128.300'],
            tableRFooter: ['', '', '', 'R519.615'],
        };
    }

    tick=()=>{
        this.setState({date: new Date()});
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
     }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        if(minutes>=10)
        {
            return (hour + ':' + minutes);
        }
        else
        {
            return (hour + ':0' + minutes);
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    render() {
        const state = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#deebf7'}}>

                {/* logo */}
                    <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
                        <Image
                            style={{width: 100, height: 100}}
                            source={{uri:'http://eurocham-cambodia.org/images/members/logos/0922b-huotraco-logo.png'}}
                        />
                    </View>
                {/* end logo */}

                {/* address */}
                    <View style={{marginTop: 10}}>
                        <Text style={styles.instructions}>
                            HUOTRACO INTERNATIONAL LIMITED 
                        </Text>
                        <Text style={styles.instructions}>
                            #299 Preah Ang Duong Boulevard, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia. 
                        </Text>
                        <Text style={styles.instructions}>
                            Tel: 023 224 992
                        </Text>
                        <Text style={styles.instructions}>
                            VAT: 98765
                        </Text>
                    </View>
                {/* end address */}

                {/* date time */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}>Date: {this.ShowCurrentDate()}</Text>
                        <Text style={{color:'black', fontSize: 10}}>Time: {this.ShowCurrentTime()}</Text>
                    </View>
                {/* end date time */}

                {/* Employee name */}
                    <Text style={styles.instructions}>
                        Employee: Latonya Ball
                    </Text>
                    <Text style={styles.stock}>
                        STOCK COLLECTED
                    </Text>
                {/* end Employee name */}

                {/* Branch ref */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style={{color:'black', fontSize: 10}}>Branch:  Kerry</Text>
                        <Text style={{color:'black', fontSize: 10}}>Ref:  #EM00004</Text>
                    </View>
                {/* end Branch Ref */}

                {/* List items */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHead} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableData} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooter} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooter} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List items */}

                {/* Employee Collecting */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Employee Collecting:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Employee Collecting */}

                {/* Store Keeper */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Store Keeper:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Store Keeper */}

                {/* Exchange Rate */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Please retain a copy for your own record and give one copy to hte store keeper.
                        </Text>
                    </View>
                    <View style={{marginBottom: 10, marginTop: 15}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Exchange rate R4,050 to $1USD  
                        </Text>
                    </View> 
                {/* End Exchange Rate */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
