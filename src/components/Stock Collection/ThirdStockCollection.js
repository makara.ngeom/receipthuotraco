/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            date: new Date(),
            // Opening Stock
            tableHeadOpeningStock: ['Opening Stock', 'QTY', 'Price', 'Total'],
            tableDataOpeningStock: [
                ['FINE KS Carton', '5', '$7.650', '$38.250'],
                ['WESTFF Carton', '10', '$3.700', '$37.000'],
                ['WESTM Carton', '3', '$3.700', '$11.100'],
            ],
            tableDFooterOpeningStock: ['TOTAL', '18', '', '$86.350'],
            tableRFooterOpeningStock: ['', '', '', 'R349,100'],
            
            // Collected Stock
            tableHeadCollectedStock: ['Collected Stock', 'QTY', 'Price', 'Total'],
            tableDataCollectedStock: [
                ['FINE KS Carton', '10', '$7.650', '$765.500'],
                ['WESTFF Carton', '8', '$3.700', '$29.600'],
                ['WESTM Carton', '6', '$3.700', '$22.200'],
            ],
            tableDFooterCollectedStock: ['TOTAL', '24', '', '$86.350'],
            tableRFooterCollectedStock: ['', '', '', 'R519,615'],

            // Total Stock
            tableHeadTotalStock: ['Total Stock', 'QTY', 'Price', 'Total'],
            tableDataTotalStock: [
                ['FINE KS Carton', '15', '$7.650', '$114.750'],
                ['WESTFF Carton', '18', '$3.700', '$66.600'],
                ['WESTM Carton', '9', '$3.700', '$33.300'],
            ],
            tableDFooterTotalStock: ['TOTAL', '42', '', '$214.650'],
            tableRFooterTotalStock: ['', '', '', '869,333'],
        };
    }

    tick=()=>{
        this.setState({date: new Date()});
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
    }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        if(minutes>=10)
        {
            return (hour + ':' + minutes);
        }
        else
        {
            return (hour + ':0' + minutes);
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
     }

    render() {
        const state = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#ebf5f9'}}>
                {/* logo */}
                    <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
                        <Image
                            style={{width: 100, height: 100}}
                            source={{uri: 'http://pngimg.com/uploads/car_logo/car_logo_PNG1667.png'}}
                        />
                    </View>
                {/* end logo */}

                {/* address */}
                    <View style={{marginTop: 10}}>
                        <Text style={styles.instructions}>
                            HUOTRACO INTERNATIONAL LIMITED 
                        </Text>
                        <Text style={styles.instructions}>
                            #299 Preah Ang Duong Boulevard, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia. 
                        </Text>
                        <Text style={styles.instructions}>
                            Tel: 023 224 992
                        </Text>
                        <Text style={styles.instructions}>
                            VAT: 98765
                        </Text>
                    </View>
                {/* end address */}

                {/* date time */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}>Date: {this.ShowCurrentDate()}</Text>
                        <Text style={{color:'black', fontSize: 10}}>Time: {this.ShowCurrentTime()}</Text>
                    </View>
                {/* end date time */}

                {/* Employee name */}
                    <Text style={styles.instructions}>
                        Employee: Latonya Ball
                    </Text>
                    <Text style={styles.stock}>
                        STOCK COLLECTED
                    </Text>
                {/* end Employee name */}

                {/* Branch ref */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style={{color:'black', fontSize: 10}}>Branch:  Kerry</Text>
                        <Text style={{color:'black', fontSize: 10}}>Ref:  #EM00004</Text>
                    </View>
                {/* end Branch Ref */}

                {/* List Items Opening Stock */}
                <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadOpeningStock} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataOpeningStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterOpeningStock} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterOpeningStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Opening Stock */}

                {/* List Items Collected Stock */}
                <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadCollectedStock} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataCollectedStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterCollectedStock} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterCollectedStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Collected Stock */}

                {/* List Items Collected Stock */}
                <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadTotalStock} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataTotalStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterTotalStock} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterTotalStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Collected Stock */}

                {/* Employee Collecting */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Employee Collecting:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Employee Collecting */}

                {/* Store Keeper */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Store Keeper:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Store Keeper */}

                {/* Exchange Rate */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Please retain a copy for your own record and give one copy to hte store keeper.
                        </Text>
                    </View>
                    <View style={{marginBottom: 10, marginTop: 15}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Exchange rate R4,050 to $1USD  
                        </Text>
                    </View> 
                {/* End Exchange Rate */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
