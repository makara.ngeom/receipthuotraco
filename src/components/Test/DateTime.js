/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }

    tick() {
        this.setState({date: new Date()});
    }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        let seconds = this.state.date.getSeconds();

        return (hour + ':' + minutes + ':' + seconds);
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
    }

    componentDidMount() {
       this.timerID = setInterval(() => this.tick(), 1000);
    }

    render() {
        return (
            <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                <Text style={{fontSize:15,color:'black'}}>{this.ShowCurrentDate()}</Text>
                <Text style={{fontSize:15,color:'black'}}>{this.ShowCurrentTime()}</Text>
            </View>
        );
    }
    }


const styles = StyleSheet.create({});
 