/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,  
    ScrollView,
    Image,
    TouchableOpacity,
} from 'react-native';
import Icons from 'native-base/node_modules/react-native-vector-icons/Entypo'

export default class SCollection extends Component {

    constructor(props){
        super(props);
        this.state = {
            count: 0,
            dataSource: [],
            date: new Date(),
            tableHead: ['Collected Stock', 'QTY', 'Price', 'Total'],
            tableData: [
                ['FINE KS Carton', '1000', '$7.650', '$76.500'],
                ['WESTFF Carton', '8', '$3.000', '$29.600'],
                ['WESTM Carton', '6', '$3.700', '$22.200'],
            ],
            tableDFooter: ['TOTAL', '24', '', '$128.300'],
            tableRFooter: ['', '', '', 'R519.615'],
            widthArr: [85, 28, 47, 55],
        };
    }

        plus(){
            this.setState({ count: this.state.count + 1 })
        }
        minus(){
            this.setState({ count: this.state.count - 1 })
        }

    render() {
       
        return (
            <View style={{flexDirection: 'row', justifyContent: 'space-around', marginBottom: 20}}>
                <View style={{width:'20%', paddingLeft: '2%', height: 100}}>
                    <Image source={{uri:'http://shivastay.com/wp-content/uploads/2018/07/small-indoor-tree-solidaria-garden-great-trees-nice-4.jpg'}} style={{width: "100%", height: 100}}/>
                </View>

                <View style={{width:'80%', height: 100, paddingLeft: '3%', flexDirection: 'column', flex: 1, justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize:16,color:'grey'}}>FINE</Text>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{width: '40%', justifyContent: 'center'}}>
                                <Text style={{fontSize:16,color:'grey'}}>Carton</Text>
                            </View>

                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <Text style={{fontSize:16,color:'grey'}}>{this.state.count}</Text>
                            </View>
                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <TouchableOpacity onPress={ ()=> this.minus() } style={{height: 30, width: 30, borderRadius: 15, borderWidth: 1,borderColor: '#000'}}>
                                    <Text style={{textAlign: 'center', textAlignVertical: 'center', flex: 1, color: 'black' }}>--</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <TouchableOpacity onPress={ ()=> this.plus() } style={{height: 30, width: 30, borderRadius: 15, borderWidth: 1,borderColor: '#000'}}>
                                    <Text style={{textAlign: 'center', textAlignVertical: 'center', flex: 1, color: 'black' }}>+</Text>
                                </TouchableOpacity>
                            </View>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{width: '40%', justifyContent: 'center'}}>
                                <Text style={{fontSize:16,color:'grey'}}>Mastercase</Text>
                            </View>

                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <Text style={{fontSize:18,color:'grey'}}>1</Text>
                            </View>
                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <TouchableOpacity style={{height: 30, width: 30, borderRadius: 15, borderWidth: 1,borderColor: '#000'}}><Text style={{textAlign: 'center', textAlignVertical: 'center', flex: 1, color: 'black' }}>--</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{width: '20%', alignItems:'center',justifyContent:'center'}}>
                                <TouchableOpacity style={{height: 30, width: 30, borderRadius: 15, borderWidth: 1,borderColor: '#000'}}>
                                    <Text style={{textAlign: 'center', textAlignVertical: 'center', flex: 1, color: 'black' }}>+</Text>
                                </TouchableOpacity>
                            </View>
                    </View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    
});
