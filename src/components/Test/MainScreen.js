/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,  
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class MainScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    dateTime(){
        Actions.dateTime();
    }

    mapLoopApi(){
        Actions.mapLoopApi();
    }

    invoice(){
        Actions.invoice();
    }

    sCollection=()=>{
        Actions.sCollection()
    }

    render() {
        return (
            <View style={{}}>
                <ScrollView style={{}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>  
                        <TouchableOpacity success style={{backgroundColor: 'yellow', padding: 15, width: '49%'}} 
                        onPress={this.dateTime}>
                            <Text style={{color:'black', textAlign: 'center', fontWeight: 'bold'}}>Date & Time</Text>
                        </TouchableOpacity>
                        <TouchableOpacity success style={{backgroundColor: 'lightblue', padding: 15, width: '49%'}} 
                        onPress={this.mapLoopApi}>
                            <Text style={{color:'black', textAlign: 'center', fontWeight: 'bold'}}>Map</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                        <TouchableOpacity success style={{backgroundColor: '#BBBBBB', padding: 15, width: '49%'}} 
                        onPress={this.invoice}>
                            <Text style={{color:'black', textAlign: 'center', fontWeight: 'bold'}}>Invoice</Text>
                        </TouchableOpacity>
                        <TouchableOpacity success style={{backgroundColor: '#9D8BCC', padding: 15, width: '49%'}} 
                        onPress={this.sCollection}>
                            <Text style={{color:'black', textAlign: 'center', fontWeight: 'bold'}}>Map</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    
});
