/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,  
    ScrollView,
    Image,
} from 'react-native';
import { Table, Row } from 'react-native-table-component';

export default class MapLoopApi extends Component {

    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            date: new Date(),
            tableHead: ['Collected Stock', 'QTY', 'Price', 'Total'],
            tableData: [
                ['FINE KS Carton', '1000', '$7.650', '$76.500'],
                ['WESTFF Carton', '8', '$3.000', '$29.600'],
                ['WESTM Carton', '6', '$3.700', '$22.200'],
            ],
            tableDFooter: ['TOTAL', '24', '', '$128.300'],
            tableRFooter: ['', '', '', 'R519.615'],
            widthArr: [85, 28, 47, 55],
        };
    }

    tick=()=>{
        this.setState({date: new Date()});
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
     }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        if(minutes>=10)
        {
            return (hour + ':' + minutes);
        }
        else
        {
            return (hour + ':0' + minutes);
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);

        const url = 'https://api.myjson.com/bins/63yjk'

        fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            dataSource: responseJson,
            isLoading: false
          })
        })
        .catch((error) => {
          console.log(error)
        })
    }

    render() {
        let sum = 5;
        const state = this.state;
        const test = this.state.dataSource;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#deebf7'}}>

                {/* logo */}
                    <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
                        <Image
                            style={{width: 100, height: 100}}
                            source={{uri:'http://eurocham-cambodia.org/images/members/logos/0922b-huotraco-logo.png'}}
                        />
                    </View>
                {/* end logo */}

                {/* address */}
                    <View style={{marginTop: 10}}>
                        <Text style={styles.instructions}>
                            HUOTRACO INTERNATIONAL LIMITED
                        </Text>
                        <Text style={styles.instructions}>
                            #299 Preah Ang Duong Boulevard, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia. 
                        </Text>
                        <Text style={styles.instructions}>
                            Tel: 023 224 992
                        </Text>
                        <Text style={styles.instructions}>
                            VAT: 98765
                        </Text>
                    </View>
                {/* end address */}

                {/* date time */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}>Date: {this.ShowCurrentDate()}</Text>
                        <Text style={{color:'black', fontSize: 10}}>Time: {this.ShowCurrentTime()}</Text>
                    </View>
                {/* end date time */}

                {/* Employee name */}
                    <Text style={styles.instructions}>
                        Employee: Latonya Ball
                    </Text>
                    <Text style={styles.stock}>
                        STOCK COLLECTED
                    </Text>
                {/* end Employee name */}

                {/* Branch ref */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style={{color:'black', fontSize: 10}}>Branch:  Kerry</Text>
                        <Text style={{color:'black', fontSize: 10}}>Ref:  #EM00004</Text>
                    </View>
                {/* end Branch Ref */}

                {/* List items */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHead} style={styles.header} textStyle={{color: 'black', fontSize: 10}} widthArr={state.widthArr}/>
                        </Table>
                        <View >
                            {
                                test.map((data, index)=>{
                                    return(
                                        <View style={{ flexDirection: 'row'}} key={index}>
                                            <Text style={{fontSize:10,color:'black', width: 85,}} flexArr={[3]}>{data.item}</Text>
                                            <Text style={{fontSize:10,color:'black', width: 30,}} flexArr={[1]}>{data.qty}</Text>
                                            <Text style={{fontSize:10,color:'black', width: 47,}} flexArr={[2]}>{data.price}</Text>
                                            <Text style={{fontSize:10,color:'black', width: 55,}} flexArr={[2]}>{data.price}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        <View >
                            {
                                <View style={{ flexDirection: 'row'}}>
                                    <Text style={{fontSize:10,color:'black', width: 85,}} flexArr={[3]}>TOTAL</Text>
                                    <Text style={{fontSize:10,color:'black', width: 30,}} flexArr={[1]}>{sum}</Text>
                                    <Text style={{fontSize:10,color:'black', width: 47,}} flexArr={[2]}></Text>
                                    <Text style={{fontSize:10,color:'black', width: 55,}} flexArr={[2]}>{sum}</Text>
                                </View>
                            }
                        </View>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableDFooter} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} widthArr={state.widthArr}/>
                            <Row data={state.tableRFooter} style={styles.text} textStyle={{color: 'black', fontSize: 10}} widthArr={state.widthArr}/>
                        </Table>
                    </View>
                {/* end List items */}

                {/* Employee Collecting */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Employee Collecting:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Employee Collecting */}

                {/* Store Keeper */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Store Keeper:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Store Keeper */}

                {/* Exchange Rate */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Please retain a copy for your own record and give one copy to hte store keeper.
                        </Text>
                    </View>
                    <View style={{marginBottom: 10, marginTop: 15}}>
                        <Text style={{color:'black', fontSize: 10}}>
                        Exchange rate R4,050 to $1USD  
                        </Text>
                    </View> 
                {/* End Exchange Rate */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
