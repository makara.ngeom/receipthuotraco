/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
  Dimensions
} from 'react-native';

let FullWidth= Dimensions.get('window').width;
const Width = FullWidth * 0.115;

export default class BorderScreen extends Component {

  constructor(props){
    super(props);
    this.state = {
    };
  }

  LoopDot = ()=>{
    var i = 0;
    let borderDot = '-';
    for(i;i<=parseInt(Width);i++)
    {
      borderDot = borderDot + ' -';
    }
    return borderDot;
  }

  render() {
    return (
      <ScrollView style={{}}>

{/* logo */}
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Image
          style={{width: 100, height: 100}}
          source={{uri: 'https://brandmark.io/logo-rank/random/pepsi.png'}}
        />
      </View>
{/* end logo */}

{/* address */}
      <View style={{padding: 10}}>
        <Text style={styles.instructions}>
          #299 Preah And Duong Boulevard, Sangkat Wat Phnom, Khan Doun Penh, Phnom Penh, Cambodia.
        </Text>
        <Text style={styles.instructions}>
          Tel: 023 224 992 
        </Text>
        <Text style={styles.instructions}>
            VAT: 98765
        </Text>

     <Text style={{fontSize:15,color:'black', textAlign: 'center', backgroundColor:'green'}}>{this.LoopDot()}</Text>
      

      </View>
{/* end address */}

{/* date time */}
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={{fontSize:15,color:'black'}}>Date: 12/20/2018</Text>
        <Text style={{fontSize:15,color:'black'}}>Time: 09:80</Text>
      </View>
{/* end date time */}

{/* Employee name */}
      <Text style={styles.instructions}>
          Employee: Latonya Ball
      </Text>
      <Text style={styles.stock}>
          STOCK COLLECTION
      </Text>
{/* end Employee name */}

{/* Branch ref */}
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
      <Text style={{fontSize:15,color:'black'}}>Branch:  Kerry</Text>
      <Text style={{fontSize:15,color:'black'}}>Ref:  #EM00004</Text>
      </View>
{/* end Branch Ref */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  stock:  {
    textAlign: 'center',
    color: '#333333',
    fontSize: 30,
    fontWeight: 'bold',
  },
  time: {
    textAlign: 'right',
    flex: 1,
    color: '#333333',
    marginBottom: 5,
  }
});
