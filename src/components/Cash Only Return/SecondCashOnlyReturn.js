/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo'
import { Table, Row, Rows } from 'react-native-table-component';

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            date: new Date(),
            // Opening Stock
            tableHeadOpeningStock: ['Opening Stock', 'QTY', 'Price', 'Total'],
            tableDataOpeningStock: [
                ['FINE KS Carton', '10', '$7.650', '$76.5000'],
                ['WESTFF Carton', '8', '$3.700', '$29.600'],
                ['WESTM Carton', '6', '$3.700', '$22.200'],
            ],
            tableDFooterOpeningStock: ['TOTAL', '24', '', '$128.300'],
            tableRFooterOpeningStock: ['', '', '', 'R519,300'],
            
            // Total Sale
            tableHeadTotalSale: ['Total Sales', 'QTY', 'Price', 'Total'],
            tableDataTotalSale: [
                ['FINE KS Carton', '8', '$7.650', '$61.200'],
                ['WESTFF Carton', '6', '$3.700', '$22.200'],
                ['WESTM Carton', '3', '$3.700', '$11.100'],
            ],
            tableDFooterTotalSale: ['TOTAL', '17', '', '$94.500'],
            tableRFooterTotalSale: ['', '', '', 'R382,725'],

            // Collected Stock
            tableHeadClosingStock: ['Closing Stock', 'QTY', 'Price', 'Total'],
            tableDataClosingStock: [
                ['FINE KS Carton', '2', '$7.650', '$15.300'],
                ['WESTFF Carton', '2', '$3.700', '$7.400'],
                ['WESTM Carton', '3', '$3.700', '$11.100'],
            ],
            tableDFooterClosingStock: ['TOTAL', '24', '', '$33.800'],
            tableRFooterClosingStock: ['', '', '', 'R136,890'],
        };
    }

    tick=()=>{
        this.setState({date: new Date()});
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
     }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        if(minutes>=10)
        {
            return (hour + ':' + minutes);
        }
        else
        {
            return (hour + ':0' + minutes);
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    render() {
        const state = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#ebf5f9'}}>
                {/* address */}
                    <View style={{marginTop: 10}}>
                        <Text style={styles.instructions}>
                            Wholesaler Name 
                        </Text>
                        <Text style={styles.instructions}>
                            Street, Village/Town, City, Cambodia. 
                        </Text>
                        <Text style={styles.instructions}>
                            Tel: 012 345 678
                        </Text>
                        <Text style={styles.instructions}>
                            VAT: 98765
                        </Text>
                    </View>
                {/* end address */}

                {/* date time */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}>Date: {this.ShowCurrentDate()}</Text>
                        <Text style={{color:'black', fontSize: 10}}>Time: {this.ShowCurrentTime()}</Text>
                    </View>
                {/* end date time */}

                {/* Employee name */}
                    <Text style={styles.instructions}>
                        Employee: Latonya Ball
                    </Text>
                    <Text style={styles.stock}>
                        CASH ONLY RETURN
                    </Text>
                {/* end Employee name */}

                {/* Branch ref */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}></Text>
                        <Text style={{color:'black', fontSize: 10}}>Ref:  #EM00004</Text>
                    </View>
                {/* end Branch Ref */}

                {/* List Items Opening Stock */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadOpeningStock} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataOpeningStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterOpeningStock} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterOpeningStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Opening Stock */}

                {/* List Items Collected Stock */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadTotalSale} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataTotalSale} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterTotalSale} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterTotalSale} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Collected Stock */}

                {/* List Items Closing Stock */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadClosingStock} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataClosingStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterClosingStock} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterClosingStock} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Closing Stock */}

                {/* No Stock Returned & Stock Carried Over */}
                    <View style={{marginTop: 10}}>
                        <Text style={{fontSize:15,color:'black', fontWeight: 'bold', textAlign: 'center'}}>
                            NO STOCK RETURNED.
                        </Text>
                        <Text style={{fontSize:15,color:'black', fontWeight: 'bold', textAlign: 'center'}}>
                            STOCK CARRIED OVER.
                        </Text>
                        <Text style={{fontSize:10,color:'black'}}>
                            Cash for the amount of 
                        </Text> 
                        <Text style={{fontSize:15,color:'black', textAlign: 'center'}}>
                            $94.500 / R 382,725  
                        </Text> 
                        <Text style={{fontSize:10,color:'black'}}>
                            returned
                        </Text>
                    </View>
                {/* end No Stock Returned & Stock Carried Over */}

                {/* Once signed by both parties */}
                    <View style={{marginTop: 10}}>
                        <Text style={{fontSize:10,color:'black'}}>
                            Once signed by both parties the wholesaler accepts responsibility of cash and stock returned.
                        </Text>
                    </View>
                {/* end Once signed by both parties */}

                {/* Store Keeper */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            HIL Representative Returning:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Store Keeper */}

                {/* Cashier */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Wholesaler Accepting Stock/Cash:  
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Cashier */}

                {/* Exchange Rate */}
                    <View style={{marginTop: 10}}>
                      <Text style={{color:'black', fontSize: 10}}>
                            One copy for Salesman 
                        </Text>
                        <Text style={{color:'black', fontSize: 10}}>
                            One copy for the Wholesaler
                        </Text>
                    </View>
                    <View style={{marginBottom: 10, marginTop: 15}}>
                        <Text style={{color:'black', fontSize: 10}}>
                            Exchange rate R4,050 to $1USD  
                        </Text>
                    </View> 
                {/* End Exchange Rate */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 18,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
