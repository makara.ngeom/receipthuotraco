import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';


import FirstStockCollection from './Stock Collection/FirstStockCollection';

import MainScreen  from './Test/MainScreen'
import DateTime from './Test/DateTime';
import MapLoopApi from './Test/MapLoopApi';
import Invoice from './Test/Invoice';
import SCollection from './Test/SCollection';

export default class Routes extends Component<{}> {
	render() {
		return(
			<Router> 
			    <Stack key="root" hideNavBar={true}>
			      <Scene key="mainScreen" component={MainScreen} title="Main Screen" initial={true}/>
						<Scene key="dateTime" component={DateTime} title="DateTime"/>
						<Scene key="mapLoopApi" component={MapLoopApi} title="MapLoopApi"/>
						<Scene key="invoice" component={Invoice} title="MapLoopApi"/>
						<Scene key="sCollection" component={SCollection} title="SCollection"/>
						
			      <Scene key="firstStockCollection" component={FirstStockCollection} title="FirstStockCollection"/>
			    </Stack> 
			</Router>
		)
	}
}