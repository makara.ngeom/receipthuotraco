/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
  Button,
  Alert
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            // Opening Stock
            tableHeadOpeningStock: ['ລ/ດ', 'ເນ ັ້ອໃນລາຍການ', 'ຈໍານວນ', 'ລາຄາຫົວໜ່ວຍ', 'ລວມເປັນເງິນ'],
            tableDataOpeningStock: [
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
                ['', '', '', '', ''],
            ],
            tableFooterOpeningStock: ['ລວມມູນຄ່າທັງໝົດ', ''],
        };
    }

    render() {
        const state = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#ebf5f9'}}>

                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                        <Image style={{width: 60, height: 25, position: 'absolute'}}
                            source={{uri: 'https://www.allbarcodesystems.com/wp-content/uploads/2017/03/free-code-39-barcode.png'}}
                        />
                    </View>

                {/* logo */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start',}}>
                            <Image style={{width: 60, height: 60}}
                                source={{uri: 'http://pngimg.com/uploads/car_logo/car_logo_PNG1667.png'}}
                            />
                        </View>
                        <Text style={{fontSize: 18, color: 'black'}}>ໃບຮບັເງນິ</Text>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 50}}>
                            <Text style={{fontSize:10,color:'black'}}>ເລກທ: ........</Text>
                        </View>
                    </View>
                {/* end logo */}

                {/* address */}
                    <View style={{paddingTop: 15}}>
                        <Text style={{fontSize:10,color:'black'}}>
                            ຜຂູ້ າຍ: ບໍລສິ ັດ ຢາສບູ ລາວ ຈໍາກດັ
                        </Text>
                        <Text style={{fontSize:10,color:'black'}}>
                            ທຕ ີ່ ງັ້ : ຖະໜນົ ທາ່ ເດ ີ່ອ ກມ.8, ບາ້ ນ ສມົ ຫວັງເໜອ , ເມ ອງ ຫາດຊາຍຟອງ
                        </Text>
                        <Text style={{fontSize:10,color:'black'}}>
                            ນະຄອນຫຼວງວຽງຈນັ ໂທລະສັບ: 021-352780
                        </Text>
                        <Text style={{fontSize:10,color:'black'}}>
                            ເລກບນັ ຊ: 01 0110 000 31329 0001 ທະນາຄານການຄາ້ ຕ່າງປະເທດລາວ
                        </Text>
                        <Text style={{fontSize:10,color:'black'}}>
                            ເລກປະຈາໍ ຕວົ ຜເູ້ ສຍອາກອນ 5870 8920 2900
                        </Text>
                    </View>
                {/* end address */}

                {/* Fill Information */}
                    <View style={{}}>
                        <Text style={{color:'black', paddingTop: 10, borderBottomWidth: 2}}></Text>
                        <Text style={{fontSize:10,color:'black', paddingTop: 10}}>ຜູູ້ຊ ັ້: ......................................................................</Text>
                        <Text style={{fontSize:10,color:'black'}}>ທ ີ່ຕັັ້ງ: ............... ບ້ານ................ ເມອງ ....................</Text>
                        <Text style={{fontSize:10,color:'black'}}>ແຂວງ: ...................................................................</Text>
                        <Text style={{fontSize:10,color:'black'}}>ເລກບັນຊ: ........................... ທະນາຄານ ..................</Text>
                        <Text style={{fontSize:10,color:'black'}}>ຮູບການຊໍາລະສະສາງ: ..............................................</Text>
                    </View>
                {/* end Fill Information */}

                {/* List Items Opening Stock */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 1, borderColor: '#666666'}}>
                            <Row data={state.tableHeadOpeningStock} style={{}} textStyle={{color: 'black', fontSize: 10, textAlign: 'center'}} flexArr={[1, 2, 2, 2, 2]}/>
                            <Rows data={state.tableDataOpeningStock} style={{height: 20}} textStyle={{color: 'black', fontSize: 10}} flexArr={[1, 2, 2, 2, 2]}/>
                            <Row data={state.tableFooterOpeningStock} style={{height: 20}} textStyle={{color: 'black', fontSize: 10, textAlign: 'right', paddingRight: 5}} flexArr={[7, 2]}/>
                        </Table>
                    </View>
                {/* end List Items Opening Stock */}

                {/* Cashier */}
                    <View style={{paddingTop: 15, paddingBottom: 30, }}>
                        <Text style={{fontSize:9,color:'black'}}>
                            *ລາຄາລວມອາກອນມູນຄ່າເພ ີ່ມແລູ້ວ
                        </Text> 
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', width: 90, marginRight: 15, fontSize: 13}}>
                            ວັນທ : ...............
                        </Text>
                        
                        <Text style={{color:'black', width: 90, marginRight: 15, fontSize: 13}}>
                            ວັນທ : ...............
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 15}}>
                    <Text style={{color:'black', width: 90, marginRight: 15, fontSize: 13, paddingLeft: 15}}>
                            ຜູູ້ຊ
                        </Text>
                        <Text style={{color:'black', width: 90, marginRight: 15, fontSize: 13, paddingLeft: 15}}>
                            ຜູູ້ຂາຍ
                        </Text>
                    </View>
                {/* End Cashier */}

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 18,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    
    MainContainer: {
    
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#F5FCFF',
        margin: 10
    
    }
});
