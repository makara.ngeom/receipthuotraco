/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo'
import { Table, Row, Rows } from 'react-native-table-component';

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            date: new Date(),
            // Opening Stock
            tableHeadSold: ['Sold', 'QTY', 'Price', 'Total'],
            tableDataSold: [
                ['FINE KS Carton', '2', '$7.650', '$15.300'],
                ['WESTFF Carton', '1', '$3.700', '$3.700'],
            ],
            tableDFooterSold: ['TOTAL', '3', '', '$19.000'],
            tableRFooterSold: ['', '', '', 'R76,3950'],
        };
    }

    tick=()=>{
        this.setState({date: new Date()});
    }

    ShowCurrentDate=()=>{
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        
        return (date + '/' + month + '/' + year); 
     }

    ShowCurrentTime=()=>{
        let hour = this.state.date.getHours();
        let minutes = this.state.date.getMinutes();
        if(minutes>=10)
        {
            return (hour + ':' + minutes);
        }
        else
        {
            return (hour + ':0' + minutes);
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    render() {
        const state = this.state;
        return (
            <View style={{alignItems: 'center'}}>
                <ScrollView style={{padding: 5, width: 220, backgroundColor: '#ebf5f9'}}>
                {/* address */}
                    <View style={{marginTop: 10}}>
                        <Text style={styles.instructions}>
                            Wholesaler Name 
                        </Text>
                        <Text style={styles.instructions}>
                            Street, Village/Town, City, Cambodia. 
                        </Text>
                        <Text style={styles.instructions}>
                            Tel: 012 345 678
                        </Text>
                        <Text style={styles.instructions}>
                            VAT: 98765
                        </Text>
                    </View>
                {/* end address */}

                {/* date time */}
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{color:'black', fontSize: 10}}>Date: {this.ShowCurrentDate()}</Text>
                        <Text style={{color:'black', fontSize: 10}}>Time: {this.ShowCurrentTime()}</Text>
                    </View>
                {/* end date time */}

                {/* Reciept Name */}
                    <Text style={styles.instructions}>
                        HIL Representative: Latonya Ball
                    </Text>
                    <Text style={styles.stock}>
                        INVOICE
                    </Text>
                {/* end Reciept Name */}

                {/* Customer info */}
                    <View style={{}}>
                        <Text style={styles.instructions}>
                            Customer ID: sfksdfksdfudf
                        </Text>
                        <Text style={styles.instructions}>
                            Customer name: iufhyiasdhfush
                        </Text>
                    </View>
                {/* end Customer info */}

                {/* List Items for Sold */}
                    <View style={styles.table}>
                        <Table borderStyle={{borderWidth: 0, backgroundColor: '#c8e1ff'}}>
                            <Row data={state.tableHeadSold} style={styles.header} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Rows data={state.tableDataSold} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableDFooterSold} style={styles.footer} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                            <Row data={state.tableRFooterSold} style={styles.text} textStyle={{color: 'black', fontSize: 10}} flexArr={[3, 1, 2, 2]}/>
                        </Table>
                    </View>
                {/* end List Items for Sold */}

                {/* Store Keeper */}
                    <View style={{}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            HIL Representative:
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Store Keeper */}

                {/* Cashier */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', marginBottom: 28, marginTop: 10, fontSize: 10}}>
                            Customer:  
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Sign
                        </Text>
                        <Text style={{color:'black', borderTopWidth: 2, width: 70, marginRight: 30, fontSize: 10, borderColor: 'lightblue'}}>
                            Print
                        </Text>
                    </View>
                {/* End Cashier */}

                {/* Exchange Rate */}
                    <View style={{marginTop: 10}}>
                        <Text style={{color:'black', fontSize: 10}}>
                            Please retain a copy for your own record and give one copy to the customer.
                        </Text>
                    </View>
                    <View style={{marginBottom: 10, marginTop: 15}}>
                        <Text style={{color:'black', fontSize: 10}}>
                            Exchange rate R4,050 to $1USD  
                        </Text>
                    </View> 
                {/* End Exchange Rate */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {
        textAlign: 'center',
        color: '#333333',
        fontSize: 18,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
