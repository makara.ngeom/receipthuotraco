/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  ScrollView,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import { Container, Header, Content, Button } from 'native-base';
import {Actions} from 'react-native-router-flux'; // used for redirect to another activity
import Route from './src/components/Routes';
// import SR from './src/components/Stock Collection/FirstStockCollection';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }
    
    render() {
        return (
            <Route/>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        fontSize: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        fontSize: 10,
    },
    stock:  {   
        textAlign: 'center',
        color: '#333333',
        fontSize: 15,
        fontWeight: 'bold',
    },
    table: { flex: 1, paddingTop: 15 },
    header: { marginTop: 1, marginBottom: 1, borderBottomWidth: 1, borderColor: 'lightblue' },
    footer: { marginTop: 1, marginBottom: 1, borderTopWidth: 1, borderColor: 'lightblue' },
    text: { marginTop: 1, marginBottom: 1, }
});
 